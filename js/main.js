var tDate=TDate();

var sunmoonobj= new Sunmoon();
var ultraviolet= new Ultraviolet();
sunmoonobj.obtainGpsPosition();
var battery= new Battery();

ultraviolet.getUvValue();
sunmoonobj.obtainFirstLocale();

var hrm=new HRM();
var checkHrm=false;
var hrmPulseTime=null;
var rInterval=0;
var rIntervalCount=0;

var showMenu=true;
var activeUV=true;
var active24h=true;
var activeBIG=true;
var heightItemMenu=79;
var itemsMenu=4;

var big=false;

function checkAvailableSensorsMenu(){
	var menuCheckHrm = hrm.HRMMotion;
	activeUV = (ultraviolet.ultravioletSensor===false)?false:true;
	var menuActiveUV = activeUV;
	
	if(!menuCheckHrm){
		var itemHRM = document.getElementById('itemHRM');
		itemHRM.style.display = "none";
		itemsMenu--;
	}
	if(!menuActiveUV){
		var itemUV = document.getElementById('itemUV');
		itemUV.style.display = "none";
		itemsMenu--;
	}
	
	var options = document.getElementById('options');
	options.style.maxHeight=(heightItemMenu*itemsMenu)+"px";
}
checkAvailableSensorsMenu();

var currentSunlight=null;
var isScreenSmall=(window.innerWidth>320)?false:true;
var W=window.innerWidth;
var H=window.innerHeight-4;
var MW=window.innerWidth;
var MH=window.innerHeight;
var sun ={r:60};
var alpha=0.2;
var G=0.008;
var rSun=75;
var rMinMoon=rSun*(2/3);
var rMedSun=rSun*(6/7);
var rMinSun=rSun*(3/5);
var rSunPlusBig=30;
var rSunInit=75;
var rMoonInit=65;
var rSunIncrease=1;
//var img=new Image();
//img.src="img/back.png";
var stars={};
var contextStars=null;
var contextBackClockPoints=null;
var canvasCurrentsClockPoints=null;
var contextCurrentsClockPoints = null;
var contextCurrentSecond = null;

var numStarts=50;
var transicion=false;
var setAnimation=null;
var easing=BezierEasing(.46,.15,.1,.94);
var startEasing=null;
var durationEasing=1500;
var textPulsar=null;
var textPulsarMonthAndDay=null;
var widthScreen = window.innerWidth;
var arrayTextPulsar=null;
var timeLapseHideMenu=null;
var craters=new Image();
craters.src="img/craters.png";
var arrayTextPulsarAux=[];
var menuActive="";
var factor=0;

var delayCurrentDayPartTime=null;
var delayCurrentDayPart=10;
var animationRevert=false;
var lastDateTransition=null;
var GA=null;

var init=true;
var noche=true;
var nocheAnterior=noche;
var sunsetOpts={dark:1,light:0};
var sunriseOpts={dark:0,light:1};
var lightOpts={dark:0,light:1,darkAnimation:0,lightAnimation:0.5};

//var img_sun=new Image();
//img_sun.src="img/back_sun.png";

var canvas= document.getElementsByTagName('canvas')[0];
var context = canvas.getContext('2d');
canvas.width=W;
canvas.height=H;

canvas.setAttribute('width',W);
canvas.setAttribute('height',H);

var planetSeconds={r:9,x:0,y:0,d:270,l:13,m:true,c:'#FFF',re:false};
var planetMinutes={r:25,x:0,y:0,d:270,l:9.5,m:true,c:'#1B59B2',re:false};

context.fillRect(0,0,W,H);
context.fillStyle   = '#000';

generateStars();

var sol= new Astro({'tipo':'sol','context':context});
var luna= new Astro({'tipo':'luna','context':context});

function beforeDraw(){
    //calculamos puntos medios MW y MH
	MW=W/2;
    if(W < 360 || (W>H)){
    	MH=H/2;
    }else{
    	MH=H/1.67;
    }
}

function draw(){
	tDate=TDate();
	
	//control de ocultación del menú
	if (timeLapseHideMenu != null && tDate.getTime() > timeLapseHideMenu.getTime()) {
		timeLapseHideMenu=null;
		
		var moreoptions = document.getElementById('moreoptions');
		var options = document.getElementById('options');
		moreoptions.className = "menu";
		options.className = "options";
		showMenu = true;
	}
	
	sunmoonobj.checkClear();
    
	checkTransitionValid();
    
    //control para volver a la parte del dia en la que estamos
    if(delayCurrentDayPartTime!= null && tDate.getTime() > delayCurrentDayPartTime.getTime()){
    	animationRevert=true;
    	delayCurrentDayPartTime=null;
    }
    
	if(!transicion && delayCurrentDayPartTime==null){
		checkNoche();
	}
	
	//init
	if((noche==nocheAnterior || init) && !transicion){
		startEasing=null;
		animationRevert=false;
		lastDateTransition=null;
		if(!noche){
			sol.resetSkyAnimation(noche);
			luna.resetSkyAnimation(noche);
			sol.drawSky();
			drawBateryLevel();
			drawBackPointsClock();
			drawPointsClock();
			drawSun(sun.r);
		}else if(noche){
			sol.resetSkyAnimation(noche);
			luna.resetSkyAnimation(noche);
			luna.drawSky();
			drawBateryLevel();
			drawBackPointsClock();
			drawPointsClock();
			drawMoon(sun.r);
		}
	}else{
		if(startEasing==null){
			startEasing=tDate.getTime();
		}
		transicion=true;
		
		if(lastDateTransition==null){
			lastDateTransition=TDate();
			lastDateTransition.setSeconds(lastDateTransition.getSeconds() + 2.2);
		}
		
		var p = (tDate.getTime()-startEasing)/durationEasing;
		var e = easing(p);
		
		if(noche){//transicion de dia a noche
			sol.subtractAnimationValue('light',0.05);
			luna.addAnimationValue('light',0.05);
			sol.setAnimationValue('limit',Math.max(Math.min((0.5-(0.5*e)),0.5),0));
			luna.setAnimationValue('limit',Math.max(Math.min((0.7*e),0.7),0));
			
			if(animationRevert){
				sol.reverseAnimationValue('limit');
				luna.reverseAnimationValue('limit');
			}
			sol.drawSky();
			luna.drawSky();
			drawBateryLevel();
			drawBackPointsClock();
			drawPointsClock();
			drawMoon(sun.r);
			drawSun(sun.r);
			if((tDate.getTime()-startEasing)>=durationEasing){
				lastDateTransition=null;
				animationRevert=false;
				sol.setAnimationValue('light',0);
				luna.setAnimationValue('light',1);
				transicion=false;
				nocheAnterior=noche;
				startEasing=null;
			}
		}else{//transicion de noche a dia
			sol.addAnimationValue('light',0.05);
			luna.subtractAnimationValue('light',0.05);
			sol.setAnimationValue('limit',Math.max(Math.min((0.7*e),0.7),0));
			luna.setAnimationValue('limit',Math.max(Math.min((0.5-(0.5*e)),0.5),0));
			if(animationRevert){
				sol.reverseAnimationValue('limit');
				luna.reverseAnimationValue('limit');
			}
			luna.drawSky();
			sol.drawSky();
			drawBateryLevel();
			drawBackPointsClock();
			drawPointsClock();
			drawMoon(sun.r);
			drawSun(sun.r);
			if((tDate.getTime()-startEasing)>=durationEasing){
				lastDateTransition=null;
				animationRevert=false;
				sol.setAnimationValue('light',1);
				luna.setAnimationValue('light',0);
				transicion=false;
				nocheAnterior=noche;
				startEasing=null;
			}
		}
	}
    drawPlanetSeconds(planetSeconds);
    drawPlanetMinutes(planetMinutes);
    
    init=false;
    if(checkHrm){
    	hrm.measureHRM();
    }
    
    window.requestAnimationFrame(draw);
}

function drawBateryLevel(){//pintamos el nivel de la batería
	battery.getBatteryLevel();
	var batteryLevel = Math.floor(batteryLvl * W);
	context.beginPath();
	var screenBrightness = (typeof tizen !=='undefined')?tizen.power.getScreenBrightness():1;
	var initHeightGradient=H-2;
	
	if(screenBrightness < 0.3){
		initHeightGradient=H-8;
	}
	context.rect(0, initHeightGradient, batteryLevel, H+4);
	var my_gradient=context.createLinearGradient(0,0,batteryLevel,0);
	if(batteryLvl > 0.5){
		my_gradient.addColorStop(0,"rgba(20,20,20,0.5)");
		my_gradient.addColorStop(1,"rgba(105,105,105,1)");
	}else{
		my_gradient.addColorStop(0,"rgba(20,20,20,0.5)");
		my_gradient.addColorStop(0.5,"rgba(90,90,90,1)");
		my_gradient.addColorStop(1,"rgba(170,170,170,1)");
	}
	
	context.fillStyle = my_gradient;
	context.fill();
	context.closePath();
	//context.restore();
}

function drawBackPointsClock(){
	if(contextBackClockPoints != null){
		context.drawImage(contextBackClockPoints.canvas, 0, 0);
	}else{
		var canvasBackClockPoints=document.createElement("CANVAS");
		canvasBackClockPoints.width=W;
		canvasBackClockPoints.height=H;
		contextBackClockPoints=canvasBackClockPoints.getContext('2d');
		
		var diameter=(W-49)/2;
		var delta=6;
		var radiusPoint=3;
		var pointS=1.2;
		var pointM=2.8;
		var pointL=6.3;
		var deltaX=0;
		var deltaY=0;
		var rotate;
		var rotateRadians;
		var second=tDate.getSeconds();
		contextBackClockPoints.save();
		contextBackClockPoints.translate(MW, MH);
		contextBackClockPoints.rotate(-(90 * Math.PI/180));
		for(var i=0;i<60;i++){
			rotate=delta*i;
			rotateRadians=rotate * Math.PI/180;
			deltaX=diameter*Math.cos(rotateRadians);
			deltaY=diameter*Math.sin(rotateRadians);
			radiusPoint=pointS;
			if(rotate % 90 == 0){
				radiusPoint=pointL;
			}else if(rotate % 15 == 0){
				radiusPoint=pointM;
			}
			
			contextBackClockPoints.beginPath();
			contextBackClockPoints.fillStyle = 'rgb(170,170,170)';
			contextBackClockPoints.arc(deltaX,deltaY, radiusPoint, 0, 2 * Math.PI, true);
			if(rotate % 90 == 0){
				contextBackClockPoints.fillStyle = 'rgb(50,50,50)';
				contextBackClockPoints.lineWidth = 3;
				contextBackClockPoints.strokeStyle = 'rgb(170,170,170)';
				contextBackClockPoints.stroke();
			}else if(rotate % 15 != 0){
				contextBackClockPoints.fillStyle = 'rgb(100,100,100)';
			}
			contextBackClockPoints.fill();
			contextBackClockPoints.closePath();
		}
		contextBackClockPoints.restore();
	}
}


function drawPointsClock(){
	var second=tDate.getSeconds();
	
	if(contextCurrentsClockPoints == null || contextCurrentSecond != second){
		if(canvasCurrentsClockPoints == null || contextCurrentsClockPoints == null){
			canvasCurrentsClockPoints=document.createElement("CANVAS");
			canvasCurrentsClockPoints.width=W;
			canvasCurrentsClockPoints.height=H;
			contextCurrentsClockPoints=canvasCurrentsClockPoints.getContext('2d');
		}
		contextCurrentsClockPoints.clearRect(0, 0, W, H);
		
		var diameter=(W-49)/2;
		var delta=6;
		var radiusPoint=3;
		var pointS=1.2;
		var pointM=2.8;
		var pointL=6.3;
		var deltaX=0;
		var deltaY=0;
		var rotate;
		var rotateRadians;
		contextCurrentsClockPoints.save();
		contextCurrentsClockPoints.translate(MW, MH);
		contextCurrentsClockPoints.rotate(-(90 * Math.PI/180));
		for(var i=0;i<60;i++){
			if(i <= second){
				rotate=delta*i;
				rotateRadians=rotate * Math.PI/180;
				deltaX=diameter*Math.cos(rotateRadians);
				deltaY=diameter*Math.sin(rotateRadians);
				radiusPoint=pointS;
				if(rotate % 90 == 0){
					radiusPoint=pointL;
				}else if(rotate % 15 == 0){
					radiusPoint=pointM;
				}
				
				contextCurrentsClockPoints.beginPath();
				radiusPoint+=0.5;
				contextCurrentsClockPoints.fillStyle = 'rgb(255,255,255)';
				contextCurrentsClockPoints.arc(deltaX,deltaY, radiusPoint, 0, 2 * Math.PI, true);
				contextCurrentsClockPoints.fill();
				contextCurrentsClockPoints.closePath();
			}else{
				break;
			}
		}
		contextCurrentsClockPoints.restore();
		contextCurrentSecond = second;
	}
	
	context.drawImage(contextCurrentsClockPoints.canvas, 0, 0);
}

function checkTransitionValid(){
	if(lastDateTransition!=null && tDate.getTime() > lastDateTransition.getTime()){
		lastDateTransition=null;
		checkNoche();
		transicion=false;
		nocheAnterior=noche;
		animationRevert=false;
		startEasing=null;
		if(noche){
			sol.setAnimationValue('light',0);
			luna.setAnimationValue('light',1);
		}else{
			sol.setAnimationValue('light',1);
			luna.setAnimationValue('light',0);
		}
	}
}

function cambioNoche(){
	delayCurrentDayPartTime=TDate();
	delayCurrentDayPartTime.setSeconds(delayCurrentDayPartTime.getSeconds() + delayCurrentDayPart);
	
	nocheAnterior=noche;
	noche=!noche;
	return true;
}

function isDay(){
	return (tDate.getTime() > sunmoondata.sun.sunrise.getTime() && tDate.getTime() < sunmoondata.sun.dusk.getTime());
}

function checkNoche(){
	nocheAnterior=noche;
	if(!isEmpty(sunmoondata)){
		if(isDay()){
			noche=false;
		}else{
			noche=true;
		}
	}else{
		noche=false;
	}
	transicion=noche!=nocheAnterior;
}

function pulsarText(){
	if(isScreenSmall){
		context.fillStyle   = '#BBB';
	}else{
		context.fillStyle   = '#999';
	}
	
    
    var mad=tDate.getDate().toString()+tDate.getMonth().toString();
    
    if(textPulsar==null || textPulsarMonthAndDay != mad){
    	textPulsarMonthAndDay=mad;
    	textPulsar=(typeof tizen !=='undefined')?tizen.time.getCurrentDateTime().toLocaleDateString():tDate.toLocaleString();
    	if(textPulsar.toLowerCase()=="invalid date"){
    		textPulsar=(typeof tizen !=='undefined')?tizen.time.getCurrentDateTime().toDateString():tDate.toLocaleString();
    	}
    	
    	arrayTextPulsarAux=textPulsar.split(',');
    	if(arrayTextPulsarAux.length > 1){
    		arrayTextPulsarAux.splice(0,1);
    	}
    	textPulsar=arrayTextPulsarAux.join(',');
    	arrayTextPulsarAux=new Array(textPulsar);
    	
    	
    	arrayTextPulsar=[];
    	for(var i=0;i<arrayTextPulsarAux.length;i++){
    		arrayTextPulsar[i]={'size':30,'text':arrayTextPulsarAux[i]};
    	}
    }
    
    var textHeight=88;
    
    if(isScreenSmall){
    	if(tDate.getMinutes() > 45 || tDate.getMinutes() < 15){
    		textHeight=H-58;
    	}else{
    		textHeight=68;
    	}
    }
    
    
    
    for(var i=0;i<arrayTextPulsar.length;i++){
    	context.font = 'bold '+arrayTextPulsar[i].size+'px resamitzregular';
    	var metrics=context.measureText(arrayTextPulsar[i].text);
		
    	if(isScreenSmall){
    		if((metrics.width+160) >=  widthScreen){
    			while((metrics.width+140) >=  widthScreen){
    				arrayTextPulsar[i].size--;
    				context.font = 'bold '+arrayTextPulsar[i].size+'px resamitzregular';
    				var metrics=context.measureText(arrayTextPulsar[i].text);
    			}
    		}
        }else{
        	if((metrics.width+50) >=  widthScreen){
        		while((metrics.width+30) >=  widthScreen){
        			arrayTextPulsar[i].size--;
        			context.font = 'bold '+arrayTextPulsar[i].size+'px resamitzregular';
        			var metrics=context.measureText(arrayTextPulsar[i].text);
        		}
        	}
        }
		
		context.fillText(arrayTextPulsar[i].text,MW-(metrics.width/2),textHeight);
		//context.fillText(tDate.getMinutes()+" : "+tDate.getSeconds()+" : "+tDate.getMilliseconds(),MW-(metrics.width/2)+60,textHeight+25);
		textHeight+=40;
	};
    sunmoonobj.obtainGpsPosition();
}

var drawSun = function(r){
    //pulsar
    var milliseconds=tDate.getMilliseconds();
        
    var auxRSunPlusBig=(big==true)?rSunPlusBig:0;
    
    rSun=sol.pulseMultiplo(sol.ceilMultiplo(milliseconds),1000)+auxRSunPlusBig;
    	
	if(checkHrm){
		if(hrm.getrRInterval() > 0 && hrm.getHeartRate() >0){
			if(rIntervalCount == 2 || rIntervalCount==0){
				rInterval=hrm.getrRInterval();
				if(rIntervalCount == 2 ){
					rIntervalCount=0;
				}
			}else{
				rIntervalCount++;
			}
		}else if(hrm.getHeartRate() == 0){
			rInterval=0;
		}
	
		if(rInterval > 0){
			if(hrmPulseTime==null){
				hrmPulseTime=tDate.getTime();
				ms=1;
			}else{
				ms=tDate.getTime()-hrmPulseTime;
				if(ms >= rInterval){
					hrmPulseTime+=rInterval;
					ms=ms-(Math.floor(ms/rInterval)*rInterval);
				}
				ms=Math.max(ms,1);
			}
			rSun=sol.pulse(ms,rInterval)+auxRSunPlusBig;
		}else{
			hrmPulseTime=null;
		}
	}
    
	context.save();
	context.translate(W/2, H);
	
	var rotateTransition=0;
	if(transicion){
		if(noche){
			rotateTransition=-(luna.getAnimationValue('limit'))*Math.PI;
		}else{
			rotateTransition=(luna.getAnimationValue('limit'))*Math.PI;
		}
		context.rotate(rotateTransition);
	}
	
	var heightTimeReducer=0;
	var heightWTimeReducer=0;
	var fontSizeMax=60;
	var fontSizeMin=52;
	
	if(!checkHrm){
		context.beginPath();
	    context.fillStyle = 'rgba('+sol.getSunColor()+','+alpha*1+')';
	    context.arc(0,-(H-MH), rSun, 0, 2 * Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    context.beginPath();
	    context.fillStyle = 'rgba('+sol.getSunColor()+','+alpha*2+')';
	    context.arc(0,-(H-MH), rMedSun+auxRSunPlusBig, 0, 2 * Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    context.beginPath();
	    context.fillStyle = 'rgba('+sol.getSunColor()+','+alpha*3+')';
	    context.arc(0,-(H-MH), rMinSun+auxRSunPlusBig, 0, 2 * Math.PI, true);
	    context.closePath();
	    context.fill();
	}else{
		var HRM=hrm.getHeartRate()
		var auxRHeartBigPlus=(auxRSunPlusBig>0)?auxRSunPlusBig+20:auxRSunPlusBig;
		
		if(HRM==0 && milliseconds>500){
			context.globalAlpha = 0.5;
		}
		
		heightTimeReducer=12;
		heightWTimeReducer=17;
		fontSizeMax=52;
		fontSizeMin=50;
		
		//pintar corazon
		context.save();
		context.translate(0, -(H-MH)+15);
		context.save();
		context.rotate(-0.25*Math.PI);
		factor=0.80;
		
		rHeart=rSun+15;
		context.fillStyle = 'rgba(157,0,247,0.2)';
		context.beginPath();
	    context.rect(-(rHeart/2),-(rHeart/2),rHeart,rHeart);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc(0,-(rHeart/2)+0.2, rHeart/2, 0, Math.PI, true);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc((rHeart/2)-0.2,0, rHeart/2, 0.5*Math.PI, 1.5*Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    rHeart=rSun+5;
	    context.fillStyle = 'rgba(157,0,247,0.5)';
	    context.beginPath();
	    context.rect(-(rHeart/2),-(rHeart/2),rHeart,rHeart);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc(0,-(rHeart/2)+0.2, rHeart/2, 0, Math.PI, true);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc((rHeart/2)-0.2,0, rHeart/2, 0.5*Math.PI, 1.5*Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    rHeart=(rSun-10)-((((rSunInit+auxRHeartBigPlus)-10)-(rSun-10))*factor);
	    context.fillStyle = 'rgba(157,0,247,1)';
	    context.beginPath();
	    context.rect(-(rHeart/2),-(rHeart/2),rHeart,rHeart);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc(0,-(rHeart/2)+0.2, rHeart/2, 0, Math.PI, true);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc((rHeart/2)-0.2,0, rHeart/2, 0.5*Math.PI, 1.5*Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    context.restore();

	    if(big){
			context.font = 'bold 31px resamitzregular';
		}else{
			context.font = 'bold 24px resamitzregular';
		}
	    context.fillStyle   = '#FFF';
	    metrics=context.measureText(hrm.getHeartRate());
	    var heightPosHRM=33;
	    if(!active24h){
	    	if(big){
	    		heightPosHRM=48;
			}else{
				heightPosHRM=33;
			}
	    }else{
	    	if(big){
	    		heightPosHRM=40;
			}else{
				heightPosHRM=25;
			}
	    }
	    context.fillText(HRM,-(metrics.width/2),heightPosHRM);
	    
	    context.restore();
	    
	    context.globalAlpha = 1;
	}
    
	if(big){
		fontSizeMin-=7;
	}
	
    context.font = 'bold '+fontSizeMax+'px resamitzregular';
    context.fillStyle   = '#000';
    var hour=getHour();
    var metrics=context.measureText(hour);
    
    var metrics=context.measureText(hour);
    if(big){
    	if(metrics.width > 154){
        	context.font = 'bold '+fontSizeMin+'px resamitzregular';
        	metrics=context.measureText(hour);
        }
    }else{
    	if(metrics.width > 67){
        	context.font = 'bold '+fontSizeMin+'px resamitzregular';
        	metrics=context.measureText(hour);
        }
    }
    
    if(!active24h){
    	context.fillText(hour,0-(metrics.width/2),-(H-MH)+18-heightTimeReducer);
    	var Wtime=getTimeW();
    	if(big){
    		context.font = 'bold 25px resamitzregular';
    		heightWTimeReducer-=5;
    	}else{
    		context.font = 'bold 17px resamitzregular';
    	}
    	metrics=context.measureText(Wtime);
    	context.fillText(Wtime,0-(metrics.width/2),-(H-MH)+39-heightWTimeReducer);
    }else{
    	context.fillText(hour,0-(metrics.width/2),-(H-MH)+20-heightTimeReducer);
    }
    
    context.restore();
    
    
    if(/*isDay() &&*/ activeUV && !checkHrm){
    	ultraviolet.getUvValue();
    	if(!ultraviolet.measuring || (ultraviolet.measuring &&  milliseconds>500)){
    		//UV
    		context.lineWidth = 6;
    		var heightUV=rMedSun+auxRSunPlusBig;
    		
    		context.save();
    		context.translate(MW,MH);
    		
    		var heightArc=0;
    		
    		if(rotateTransition!=0){
    			heightArc=-(H-MH);
    			
    			context.restore();
    			context.save();
    			context.translate(W/2, H);
    			context.rotate(rotateTransition);
    		}
    		if(rotateTransition==0){
    			rotateTransition=1;
    		}
    		
    		context.lineWidth = 2.8;
    		var colorUv='rgba(26,184,0,1)';
    		if(uvValue > 4 && uvValue < 8){//amarillo
    			colorUv='rgba(255,255,0,1)';
    		}else if(uvValue > 8 && uvValue < 10){//naranja
    			colorUv='rgba(245,156,0,1)';
    		}else if(uvValue > 10 && uvValue < 14){//rojo
    			colorUv='rgba(250,17,0,1)';
    		}else if(uvValue > 14){//violeta
    			colorUv='rgba(186,0,239,1)';
    		}
    		//completa
    		context.beginPath();
    		context.strokeStyle = colorUv;
    		context.arc(0,heightArc, heightUV, 0, 2 * Math.PI, false);
    		context.stroke();
    		context.closePath();
    		
    		context.restore();
    	}
    }
};

var drawMoon = function(r) {

    //pulsar
    var milliseconds=tDate.getMilliseconds();
    var auxRSunPlusBig=(big==true)?rSunPlusBig:0;
    
	rSun=luna.pulseMultiplo(luna.ceilMultiplo(milliseconds),1000)+auxRSunPlusBig;
	
    if(checkHrm){
    	if(hrm.getrRInterval() > 0 && hrm.getHeartRate() >0){
    		if(rIntervalCount == 2 || rIntervalCount==0){
				rInterval=hrm.getrRInterval();
				if(rIntervalCount == 2 ){
					rIntervalCount=0;
				}
			}else{
				rIntervalCount++;
			}
    	}else if(hrm.getHeartRate() == 0){
    		rInterval=0;
    	}

    	if(rInterval > 0){
    		if(hrmPulseTime==null){
    			hrmPulseTime=tDate.getTime();
    			ms=1;
    		}else{
    			ms=tDate.getTime()-hrmPulseTime;
    			if(ms > rInterval){
    				hrmPulseTime+=rInterval;
    				ms=ms-(Math.floor(ms/rInterval)*rInterval);
    			}
    			ms=Math.max(ms,1);
    		}
    		rSun=luna.pulse(ms,rInterval)+auxRSunPlusBig;
    	}else{
    		hrmPulseTime=null;
    	}
    }
    
    
	context.save();
	
	context.translate(W/2, H);
	if(transicion){
		if(noche){
			context.rotate((sol.getAnimationValue('limit'))*Math.PI);
		}else{
			context.rotate(-(sol.getAnimationValue('limit'))*Math.PI);
		}
	}
	
	var heightTimeReducer=0;
	var heightWTimeReducer=0;
	var fontSizeMax=62;
	var fontSizeMin=55;
	
	if(!checkHrm){
		context.beginPath();
	    context.fillStyle = 'rgba(139,131,196,0.2)';
	    context.arc(0,-(H-MH), rSun, 0, 2 * Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    context.beginPath();
	    context.fillStyle = 'rgba(124,124,180,0.4)';
	    context.arc(0,-(H-MH), rMinMoon+auxRSunPlusBig, 0, 2 * Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    context.beginPath();
	    context.fillStyle = 'rgba(148,148,190,1)';
	    
	    var delta=sunmoondata.moonfase*2;
	    if(sunmoondata.latitude < 0){
	    	delta+=1;
	    }
	    if(sunmoondata.moonfase > 0.5){
	    	delta+=1;
	    }	
	    
	    //pintamos las fases lunares
	    var radians=delta*Math.PI;
	    var radiansOposite = (2-delta)*Math.PI;
	    var radiansAux;
	    if(sunmoondata.moonfase > 0.5){
	    	radiansAux=radians;
	    	radians=radiansOposite;
	    	radiansOposite=radiansAux;
	    }
	    context.arc(0,-(H-MH), rMinMoon+auxRSunPlusBig,  radians, radiansOposite, true);
	    
	    
	    context.fill();
	    
	    context.save();
	    context.clip();
	    context.drawImage(craters, -(rMinMoon+auxRSunPlusBig), -(H-MH)-(rMinMoon+auxRSunPlusBig), (rMinMoon+auxRSunPlusBig)*2, (rMinMoon+auxRSunPlusBig)*2);
	    context.restore();
	    
	    context.closePath();
	    
	}else{
		var HRM=hrm.getHeartRate();
		var auxRHeartBigPlus=(auxRSunPlusBig>0)?auxRSunPlusBig+20:auxRSunPlusBig;
		
		if(HRM==0 && milliseconds>500){
			context.globalAlpha = 0.5;
		}

		heightTimeReducer=14;
		heightWTimeReducer=18;
		fontSizeMax=50;
		fontSizeMin=50;
		
		//pintar corazon
		context.save();
		context.translate(0, -(H-MH)+15);
		context.save();
		context.rotate(-0.25*Math.PI);
		factor=0.80;
		
		rHeart=rSun+25;
		context.fillStyle = 'rgba(157,0,247,0.2)';
		context.beginPath();
	    context.rect(-(rHeart/2),-(rHeart/2),rHeart,rHeart);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc(0,-(rHeart/2)+0.2, rHeart/2, 0, Math.PI, true);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc((rHeart/2)-0.2,0, rHeart/2, 0.5*Math.PI, 1.5*Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    rHeart=rSun+15;
	    context.fillStyle = 'rgba(157,0,247,0.5)';
	    context.beginPath();
	    context.rect(-(rHeart/2),-(rHeart/2),rHeart,rHeart);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc(0,-(rHeart/2)+0.2, rHeart/2, 0, Math.PI, true);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc((rHeart/2)-0.2,0, rHeart/2, 0.5*Math.PI, 1.5*Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    rHeart=rSun-(((rMoonInit+auxRHeartBigPlus)-rSun)*factor);
	    context.fillStyle = 'rgba(157,0,247,1)';
	    context.beginPath();
	    context.rect(-(rHeart/2),-(rHeart/2),rHeart,rHeart);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc(0,-(rHeart/2)+0.2, rHeart/2, 0, Math.PI, true);
	    context.closePath();
	    context.fill();
	    context.beginPath();
	    context.arc((rHeart/2)-0.2,0, rHeart/2, 0.5*Math.PI, 1.5*Math.PI, true);
	    context.closePath();
	    context.fill();
	    
	    context.restore();
	    
		if(big){
			context.font = 'bold 32px resamitzregular';
		}else{
			context.font = 'bold 25px resamitzregular';
		}
	    context.fillStyle   = '#FFF';
	    metrics=context.measureText(hrm.getHeartRate());
	    var heightPosHRM=33;
	    if(!active24h){
	    	if(big){
	    		heightPosHRM=48;
			}else{
				heightPosHRM=33;
			}
	    }else{
	    	if(big){
	    		heightPosHRM=40;
			}else{
				heightPosHRM=25;
			}
	    }
	    context.fillText(HRM,-(metrics.width/2),heightPosHRM);
	    context.restore();
	    
	    context.globalAlpha = 1;
	}
	
	if(big){
		fontSizeMin-=7;
	}
	
	context.font = 'bold '+fontSizeMax+'px resamitzregular';
    context.fillStyle   = '#000';
    var hour=getHour();
    var metrics=context.measureText(hour);
    if(big){
    	if(metrics.width > 154){
        	context.font = 'bold '+fontSizeMin+'px resamitzregular';
        	metrics=context.measureText(hour);
        }
    }else{
    	if(metrics.width > 67){
        	context.font = 'bold '+fontSizeMin+'px resamitzregular';
        	metrics=context.measureText(hour);
        }
    }
    
    if(!active24h){
    	context.fillText(hour,0-(metrics.width/2),-(H-MH)+18-heightTimeReducer);
    	var Wtime=getTimeW();
    	if(big){
    		heightWTimeReducer-=5;
    		context.font = 'bold 25px resamitzregular';
    	}else{
    		context.font = 'bold 17px resamitzregular';
    	}
    	metrics=context.measureText(Wtime);
    	context.fillText(Wtime,0-(metrics.width/2),-(H-MH)+39-heightWTimeReducer);
    }else{
    	context.fillText(hour,0-(metrics.width/2),-(H-MH)+20-heightTimeReducer);
    }
    
    context.restore();
};

function getHour(){
	var hours=tDate.getHours();
    if (!active24h) {
    	hours = hours % 12;
    	hours = hours ? hours : 12;
    }
    
    if(hours<10){
    	hours="0"+hours;
    }
    if(big){
    	var minutes=tDate.getMinutes();
    	if(minutes<10){
    		minutes="0"+minutes;
    	}
    	hours+=":"+minutes;
    }
    
    return hours;
}

function getTimeW(){
    var hours=tDate.getHours();
    var W = hours >= 12 ? 'PM' : 'AM';
    
    return W;
}

function getDegreeSeconds(){
    var milliseconds=tDate.getMilliseconds()/1000;
    milliseconds=parseFloat(milliseconds.toFixed(2));

    var seconds=tDate.getSeconds()+milliseconds;
    seconds+=15;
    if(seconds > 60){
        seconds=seconds-60;
    }
    return ((seconds*6)+180);
}

function getDegreeMinutes(){
    var seconds=tDate.getSeconds()/60;
    var minutes=tDate.getMinutes()+seconds;

    minutes+=15;
    if(minutes > 60){
        minutes=minutes-60;
    }

    return ((minutes*6)+180);
}

var drawPlanetSeconds = function(planet){
	var delta=getDegreeSeconds();
	var radians=delta * Math.PI/180;
	var deltaX=Math.cos(radians)*((W-49)/2);
	var deltaY=Math.sin(radians)*((W-49)/2);
	
	var comet1 =radians;
	var comet2 =radians;
	var comet3 =radians;
    
    context.beginPath();
    context.fillStyle = planet.c;
    context.arc(deltaX+MW,deltaY+MH, planet.r, 0, 2 * Math.PI, true);
    context.closePath();
    context.fill();
}

var drawPlanetMinutes = function(planet){
    if(!big){
		context.beginPath();
	    context.fillStyle = planet.c;
	    var delta=getDegreeMinutes();
	
	    var radians=delta * Math.PI/180;
	    if(isScreenSmall){
	    	var deltaX=Math.cos(radians)*((W-115)/2);
	    	var deltaY=Math.sin(radians)*((W-115)/2);
	    }else{
	    	var deltaX=Math.cos(radians)*((W-130)/2);
	    	var deltaY=Math.sin(radians)*((W-130)/2);
	    }
	    planet.d--;
	    
	    if(planet.r>0){
	        if(isScreenSmall){
	        	context.arc(deltaX+MW,deltaY+MH, planet.r-5, 0, 2 * Math.PI, true);
	        }else{
	        	context.arc(deltaX+MW,deltaY+MH, planet.r, 0, 2 * Math.PI, true);
	        }
	    }
	    context.fill();
		
		var minutes=Math.round(tDate.getMinutes());
		if(minutes<10){
			minutes="0"+minutes;
		}
	    if(isScreenSmall){
	    	context.font = 'bold 22px resamitzregular';
	    }else{
	    	context.font = 'bold 27px resamitzregular';
	    }
	    
	    context.fillStyle   = '#FFF';
	    var metrics=context.measureText(minutes);
	    
	    context.fillText(minutes,deltaX+MW-(metrics.width/2)-1,deltaY+MH+(metrics.width/3.4));
    }
}

/*draw();
setAnimation=setInterval(function(){
    draw();
},70);*/

beforeDraw();
window.requestAnimationFrame(draw);

function drawStars(){
	if(contextStars == null){
		var canvasStars=document.createElement("CANVAS");
		canvasStars.width=W;
		canvasStars.height=H;
		contextStars=canvasStars.getContext('2d');
		contextStars.fillStyle = '#777';
		
		for (var i=0; i < numStarts; i++) {
			contextStars.beginPath();
			var diameter=stars[i].diameter;
			contextStars.arc(stars[i].x,stars[i].y, diameter, 0, 2 * Math.PI, true);
			contextStars.closePath();
			contextStars.fill();
		};
		
	}else{
		context.drawImage(contextStars.canvas,0,0);
		if(boolRandom()){
			context.fillStyle = '#777';
			var star=stars[parseInt(Math.random()*numStarts)];
			var star2=stars[parseInt(Math.random()*numStarts)];
			
			var diameter=star.diameter+(Math.random()*1.5);
			context.beginPath();
			context.arc(star.x,star.y, diameter, 0, 2 * Math.PI, true);
			context.arc(star2.x,star2.y, diameter, 0, 2 * Math.PI, true);
			context.closePath();
			context.fill();
		}
	}
	
	/*context.save();
	context.translate(0, 0);
	context.fillStyle = '#777';
	for (var i=0; i < numStarts; i++) {
		context.beginPath();
		var diameter=stars[i].diameter;
		if(boolRandom()){
			diameter=diameter+(Math.random()*1.5);
		}
		context.arc(stars[i].x,stars[i].y, diameter, 0, 2 * Math.PI, true);
	    context.closePath();
	    context.fill();
	};	
	context.restore();*/
}

function generateStars(){
	
	for (var i=0; i < numStarts; i++) {
	  var diameter=Math.random()*2;
	  
	  var x=Math.floor(Math.random()*W);
	  var y=Math.floor(Math.random()*H);
	  
	  while(Math.sqrt(Math.pow((x-W/2),2) + Math.pow((y-H/1.7),2)) < 90){
		  var x=Math.random()*W;
		  var y=Math.random()*H;
	  }
	  
	  stars[i]={diameter:diameter,x:x,y:y};
	};	
}


/*************************/
/**** LISTENER EVENTS ****/
/*************************/
var changedCallback = function(){ 
    try {
        tDate=TDate();
    } catch (err) {
        console.log (err.name + ": " + err.message);
    }
};

tizen.time.setDateTimeChangeListener(changedCallback);

var canvasEvent = document.getElementById('canvas');
var move = 0;

var moreoptions = document.getElementById('moreoptions');
var options = document.getElementById('options');
var checkboxuv = document.getElementById('checkboxuv');
var checkbox24h = document.getElementById('checkbox24h');
var checkboxHRM = document.getElementById('checkboxHRM');
var checkboxBIG = document.getElementById('checkboxBIG');

moreoptions.addEventListener('touchstart', function(e) {
	menuActive=(' '+moreoptions.className+' ').indexOf(' active ') > -1;
	//if(showMenu){
	if(!menuActive){
		showMenu = false;
		moreoptions.className = "menu active";
		options.className = "options show";
		
		timeLapseHideMenu=TDate();
		timeLapseHideMenu.setSeconds(timeLapseHideMenu.getSeconds() + 15);
		//checkboxuv.disabled=!isDay();
	}else{
		showMenu = true;
		timeLapseHideMenu = null;
		moreoptions.className = "menu";
		options.className = "options";
	}
}, false);

checkbox24h.addEventListener('change', function(e) {
	active24h=!!checkbox24h.checked;
}, false);

checkboxuv.addEventListener('change', function(e) {
	if(checkboxuv.checked){
		activeUV=true;
	}else{
		ultraviolet.clearUv();
		activeUV=false;
	}
}, false);

checkboxHRM.addEventListener('change', function(e) {
	if(checkboxHRM.checked){
		checkHrm=true;
	}else{
		hrm.stop();
		checkHrm=false;
	}
}, false);

checkboxBIG.addEventListener('change', function(e) {
	sol.resetPulses();
	luna.resetPulses();
	
	if(checkboxBIG.checked){
		big=true;
	}else{
		big=false;
	}
}, false);

canvasEvent.addEventListener('touchstart', function(e) {
	var moreoptions = document.getElementById('moreoptions');
	var options = document.getElementById('options');
	moreoptions.className = "menu";
	options.className = "options";
	showMenu = false;
	
	move = 0;
}, false);
canvasEvent.addEventListener('touchmove', function(e) {
	move++;
}, false);
canvasEvent.addEventListener('touchend', function(e) {
	if (!checkHrm && !transicion && move < 6) {
		cambioNoche();
	}
}, false);
