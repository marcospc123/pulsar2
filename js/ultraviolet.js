function Ultraviolet(){
	var obj=this;
	this.timesShortUpdate=0;
	this.ultravioletAvailableSensor = (typeof webapis != 'undefined' && typeof webapis.sensorservice != 'undefined' && typeof webapis.sensorservice.getAvailableSensors != 'undefined')?webapis.sensorservice.getAvailableSensors():[];
	if(this.ultravioletAvailableSensor.length>0){
		if(this.ultravioletAvailableSensor.indexOf("ULTRAVIOLET") != -1){
			this.ultravioletSensor = (typeof webapis != 'undefined')?webapis.sensorservice.getDefaultSensor("ULTRAVIOLET"):false;
		}else{
			this.ultravioletSensor = false;
		}
	}else{
		this.ultravioletSensor=false;
	}
	
	this.lastDateUpdate=TDate();
	this.measuring=false;
	this.stop=false;
	this.maxValue=0;
	
	//this.lastDateUpdate.setSeconds(this.lastDateUpdate.getSeconds() + 10);
	
	this.getUvValue = function(){
		if(this.ultravioletSensor===false){
			return;
		}
		if (tDate.getTime() > obj.lastDateUpdate.getTime()) {
			obj.measuring=true;
			if(obj.timesShortUpdate > 2){
				if(uvValue>3){
					obj.measuring=false;
					this.lastDateUpdate=TDate();
					this.lastDateUpdate.setSeconds(this.lastDateUpdate.getSeconds() + 60);
					obj.timesShortUpdate=0;
					obj.stop=true;
					obj.ultravioletSensor.stop();
				}else if(uvValue>1.5 || obj.timesShortUpdate > 5){
					obj.measuring=false;
					this.lastDateUpdate=TDate();
					this.lastDateUpdate.setSeconds(this.lastDateUpdate.getSeconds() + 25);
					obj.timesShortUpdate=0;
					obj.stop=true;
					obj.ultravioletSensor.stop();
				}else{
					this.lastDateUpdate=TDate();
					this.lastDateUpdate.setSeconds(this.lastDateUpdate.getSeconds() + 1.2);
					obj.timesShortUpdate++;
					obj.stop=false;
				}
			}else{
				if(obj.timesShortUpdate  == 0){
					obj.maxValue=0;
				}
				this.lastDateUpdate=TDate();
				this.lastDateUpdate.setSeconds(this.lastDateUpdate.getSeconds() + 1);
				obj.timesShortUpdate++;
				obj.stop=false;
			}
			
			obj.ultravioletSensor.start(function(){
				obj.ultravioletSensor.getUltravioletSensorData(function(sensorData){
					var uvAux = sensorData.ultravioletLevel;
					if(uvAux > obj.maxValue){
						obj.maxValue=uvAux;
					}
					uvValue = Math.max(obj.maxValue,0.5);
					
					
					//uvValue = Math.max(uvAux,15);
					
					if(obj.stop){
						obj.ultravioletSensor.stop();
					}
				}, function(error){
					uvValue = 0;
				});
			});
			
		}
	};
	
	this.clearUv = function(){
		obj.lastDateUpdate=TDate();
		obj.ultravioletSensor.stop();
		obj.timesShortUpdate=0;
	};
	
	document.addEventListener("visibilitychange", pageVisibilityHandler, false);
	function pageVisibilityHandler() 
	{
		if (document.hidden) 
		{
			if(obj.measuring){
				obj.measuring=false;
				obj.ultravioletSensor.stop();
			}
		}else{
			obj.lastDateUpdate=TDate();
		}
	}
	
};
