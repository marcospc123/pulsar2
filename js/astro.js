function Astro(options){
	var obj=this;
	obj.tipos={'1':'sol','2':'luna'};
	obj.tipo=options.tipo;
	obj.options=options;
	obj.solOptions = {
		radio:75,
	};
	obj.initOnce=false;
	
	var img_prefix=window.innerWidth>320?'':'_small';
	
	obj.lunaSky=new Image();
	obj.lunaSky.src="img/back"+img_prefix+".png";
	obj.solSky=new Image();
	obj.solSky.src="img/back_sun"+img_prefix+".png";
	
	obj.astroOptions={
		'sol':{
			radio:75,
			radioBig:85,
			pulses:{},
			defaultColor:'#302903',
			colors:{
				'sunrise':'253,225,0',
				'sunriseEnd':'252,233,0',
				'goldenHourEnd':'255,255,0',
				'solarNoon':'255,255,0',
				'goldenHour':'255,255,0',
				'sunsetStart':'254,244,0',
				'sunset':'253,233,0',
				'dusk':'253,225,0',
				
				'nauticalDusk':'255,255,0',
				'night':'255,255,0',
				'nadir':'255,255,0',
				'nightEnd':'255,255,0',
				'nauticalDawn':'255,255,0',
				'dawn':'255,255,0'
			},
			sky:{
				img:obj.solSky,
				bg:{
					defaultColor:'#302903',
					colors:{
						'sunrise':'#3c2908',
						'sunriseEnd':'#392b04',
						'goldenHourEnd':'#3b3105',
						'solarNoon':'#413b07',
						'goldenHour':'#3b3305',
						'sunsetStart':'#3a2f05',
						'sunset':'#392b04',
						'dusk':'#3c2908',
						
						'nauticalDusk':'#232004',
						'night':'#232004',
						'nadir':'#232004',
						'nightEnd':'#232004',
						'nauticalDawn':'#232004',
						'dawn':'#232004'
					},
					canvas:null,
					img:null,
					timestamp:null,
				},
				bgHeart:{
					color:'#280749',
					canvas:null,
					img:null,
					timestamp:null,
				}
			},
			animation:{
				light:1,
				limit:0.5,
			}
		},
		'luna':{
			radio:65,
			radioBig:72,
			pulses:{},
			sky:{
				img:obj.lunaSky,
				bg:{
					defaultColor:'#160033',
					canvas:null,
					img:null,
					timestamp:null,
				},
				bgHeart:{
					color:'#280749',
					canvas:null,
					img:null,
					timestamp:null,
				}
			},
			animation:{
				light:1,
				limit:0,
			}
		}
	};
	
	obj.setSol = function(){
		obj.tipo=obj.tipos[1];
	};

	obj.setLuna = function(){
		obj.tipo=obj.tipos[2];
	};
	
	obj.getTipo= function(){
		alert(obj.tipo);
	};
	
	obj.init=function(){
		//creamos canvas auxiliares
		if(obj.initOnce==false){
			obj.initOnce=true;
			var canvasAstro=document.createElement("CANVAS");
			canvasAstro.width=W;
			canvasAstro.height=H;
			obj.astroOptions[obj.tipo].sky.bg.canvas=canvasAstro.getContext('2d');
			
			var canvasHeart=document.createElement("CANVAS");
			canvasHeart.width=W;
			canvasHeart.height=H;
			obj.astroOptions[obj.tipo].sky.bgHeart.canvas=canvasHeart.getContext('2d');
		}
	};
	
	obj.drawGradient=function(){
		//tDate=TDate();
		if(obj.astroOptions[obj.tipo].sky.bg.timestamp == null || tDate.getTime() > obj.astroOptions[obj.tipo].sky.bg.timestamp.getTime()){
			var sDate=TDate();
			
			//var arrayTimes=SunCalc.getTimes(sDate, sunmoondata.latitude, sunmoondata.longitude);
			var arrayTimes=sunmoondata.sun;
			
			currentSunlight=null;
			for (var key in arrayTimes) {
				var diference=arrayTimes[key].getTime()-sDate.getTime();
				if( (currentSunlight==null && diference > 0) || (diference==0) || (currentSunlight!=null && currentSunlight.diference > diference && diference > 0)){
					currentSunlight={key:key,diference:diference,time:arrayTimes[key]};
				}
			}
			
			if(currentSunlight==null){
				currentSunlight={key:'night',diference:0,time:arrayTimes["night"]};
			}
			
			obj.astroOptions[obj.tipo].sky.bg.timestamp=TDate();
			obj.astroOptions[obj.tipo].sky.bg.timestamp.setSeconds(obj.astroOptions[obj.tipo].sky.bg.timestamp.getSeconds() + 180);
			var contextGrd=obj.astroOptions[obj.tipo].sky.bg.canvas;
			var grd = contextGrd.createRadialGradient(MW, MH, ((W)/3.5), MW, MH, ((W-5)/2));
			//grd.addColorStop(0, obj.astroOptions[obj.tipo].sky.bg.defaultColor);
			var bgColor=(typeof obj.astroOptions[obj.tipo].sky.bg.colors != 'undefined')?obj.astroOptions[obj.tipo].sky.bg.colors[currentSunlight.key]:obj.astroOptions[obj.tipo].sky.bg.defaultColor;
			
			grd.addColorStop(0, bgColor);
			grd.addColorStop(1, '#000000');
			contextGrd.fillStyle = grd;
			contextGrd.fillRect(0,0,W,H);
			
			var contextGrdHeart=obj.astroOptions[obj.tipo].sky.bgHeart.canvas;
			var grd = contextGrdHeart.createRadialGradient(MW, MH, ((W)/4), MW, MH, ((W-5)/2));
			grd.addColorStop(0, obj.astroOptions[obj.tipo].sky.bgHeart.color);
			grd.addColorStop(1, '#000000');
			contextGrdHeart.fillStyle = grd;
			contextGrdHeart.fillRect(0,0,W,H);
		}
	};
	
	obj.getSunColor = function(){
		return (typeof obj.astroOptions[obj.tipo].colors != 'undefined') ? obj.astroOptions[obj.tipo].colors[currentSunlight.key] : obj.astroOptions[obj.tipo].defaulColor;
	};
	
	obj.pulseMultiplo = function(curTime,limitTime){
		var pulse;
		if(typeof obj.astroOptions[obj.tipo].pulses[curTime] != 'undefined'){
			pulse = obj.astroOptions[obj.tipo].pulses[curTime];
		}else{
			pulse = obj.pulse(curTime,limitTime);
			obj.astroOptions[obj.tipo].pulses[curTime]=pulse;
		}
		
		return pulse;
	};
	obj.factorPulse = function(curTime,limitTime){
		//curTime global var
		curTime = Math.max(Math.min(curTime,(limitTime-1)),1);
		var MLimitTime=limitTime/2;
		if(curTime > MLimitTime){
	    	var percent=((limitTime-curTime)/MLimitTime);
	    }else if(curTime <= MLimitTime){
	    	var percent=(curTime/MLimitTime);
	    }
		return percent;
	};
	
	//generar pulso del astro////////
	obj.pulse = function(curTime,limitTime){
		//curTime global var
		curTime = Math.max(Math.min(curTime,(limitTime-1)),1);
		var MLimitTime=limitTime/2;
		if(curTime > MLimitTime){
			var percent=((limitTime-curTime)/MLimitTime);
		}else if(curTime <= MLimitTime){
			var percent=(curTime/MLimitTime);
		}
		if(big){
			return obj.astroOptions[obj.tipo].radioBig+(8*percent);
		}else{
			return obj.astroOptions[obj.tipo].radio+(8*percent);
		}
	};
	
	obj.resetPulses = function(){
		obj.astroOptions[obj.tipo].pulses=[];
	}
	
	obj.ceilMultiplo = function(n){
		if(n <= 0){
	        return 0;
	    }else if(n > 0){
	        return Math.ceil(n/25) * 25;
	    }else{
	        return 25;
	    }
	};
	
	//pintar cielo////////////////////
	obj.drawSky = function(){
		obj.drawGradient();
		
		
		context.save();
		context.globalAlpha=Math.max(Math.min(obj.astroOptions[obj.tipo].animation.light,1),0);
		context.fill();
	    context.fillStyle   = '#000';
	    context.fillRect(0,0,W,H);
	    //context.drawImage(obj.astroOptions[obj.tipo].sky.img,0,-14);
		
	    /*var now=new Date();
	    var pulse=obj.factorPulse(now.getMilliseconds(),1000);
	    var resizeW=(pulse*50)+(W);
	    var resizeH=(pulse*50)+(H);
	    var posX=-((resizeW-W)/2);
	    var posY=-((resizeH-H)/2)*(1.1);*/
	    
	    
	    if(!checkHrm){
	    	//context.drawImage(obj.astroOptions[obj.tipo].sky.bg.canvas.canvas,0,0,W,H,posX,posY,resizeW,resizeH);
	    	context.drawImage(obj.astroOptions[obj.tipo].sky.bg.canvas.canvas,0,0);
		}else{
			//context.drawImage(obj.astroOptions[obj.tipo].sky.bgHeart.canvas.canvas,0,0,W,H,posX,posY,resizeW,resizeH);
			context.drawImage(obj.astroOptions[obj.tipo].sky.bgHeart.canvas.canvas,0,0);
		}
	    drawStars();
		context.restore();
		pulsarText();
		
		//PINTAMOS EN QUE PARTE DEL DIA ESTAMOS
		/*context.fillStyle = "#FFFFFF";
		context.font = 'bold 15px resamitzregular';
		context.fillText(currentSunlight.key+" - "+currentSunlight.diference,15,15);*/
		
		/*context.save();
		context.globalAlpha=Math.max(Math.min(obj.astroOptions[obj.tipo].animation.light,1),0);
		context.fill();
		var grd = context.createRadialGradient(MW, MH, ((W)/3.5), MW, MH, ((W-5)/2));
		grd.addColorStop(0, obj.astroOptions[obj.tipo].sky.bg.color);
		grd.addColorStop(1, '#000000');
		context.fillStyle = grd;
	    context.fillRect(0,0,W,H);
	    //context.drawImage(obj.astroOptions[obj.tipo].sky.img,0,-14);
	    drawStars();
		context.restore();
		pulsarText();*/
		
	};
	
	obj.setAnimationValue = function(param,value){
		obj.astroOptions[obj.tipo].animation[param]=value;
	}
	
	obj.getAnimationValue = function(param){
		return obj.astroOptions[obj.tipo].animation[param];
	}
	
	obj.addAnimationValue = function(param,value){
		obj.astroOptions[obj.tipo].animation[param]+=value;
	}
	
	obj.subtractAnimationValue = function(param,value){
		obj.astroOptions[obj.tipo].animation[param]-=value;
	}
	
	obj.reverseAnimationValue = function(param){
		obj.astroOptions[obj.tipo].animation[param]*=(-1);
	}
	
	obj.resetSkyAnimation = function(noche){
		if(!noche){
			obj.astroOptions['sol'].animation.light=1;
			obj.astroOptions['sol'].animation.limit=0.5;
				
			obj.astroOptions['luna'].animation.light=0;
			obj.astroOptions['luna'].animation.limit=0;
		}else{
			obj.astroOptions['sol'].animation.light=0;
			obj.astroOptions['sol'].animation.limit=0;
			
			obj.astroOptions['luna'].animation.light=1;
			obj.astroOptions['luna'].animation.limit=0.5;
		}
	}

	obj.init();
};

 