function Sunmoon(){
	var obj=this;
	this.locale="ES";
	this.latitude=countries[this.locale].latitude;
	this.longitude=countries[this.locale].longitude;
	
	this.datetime=TDate();
	//this.datetime=new Date(Date.UTC(this.datetime.getFullYear(), this.datetime.getMonth(), this.datetime.getDate(), this.datetime.getHours(), this.datetime.getMinutes(), this.datetime.getSeconds(), this.datetime.getMilliseconds()));
	this.sun=SunCalc.getTimes(this.datetime,this.latitude,this.longitude);
	this.moonFase=SunCalc.getMoonIllumination(this.datetime);
	this.lastDateUpdate=TDate();
	this.lastDateUpdate.setSeconds(this.lastDateUpdate.getSeconds() + 10);
	this.typeLocation="D";
	this.watchId=null;
	this.watchDate=null;
	
	this.data={'sun':this.sun,'moonfase':this.moonFase.phase,'latitude':this.latitude,'longitude':this.longitude,'lastDateUpdate':this.lastDateUpdate,'typeLocation':this.typeLocation};
	sunmoondata=this.data;
	this.setPositionData=function(moreTime){
		obj.datetime=TDate();
		//obj.datetime=new Date(Date.UTC(obj.datetime.getFullYear(), obj.datetime.getMonth(), obj.datetime.getDate(), obj.datetime.getHours(), obj.datetime.getMinutes(), obj.datetime.getSeconds(),obj.datetime.getMilliseconds()));
		obj.sun=SunCalc.getTimes(obj.datetime,obj.latitude,obj.longitude);
		obj.moonFase=SunCalc.getMoonIllumination(obj.datetime);
		obj.lastDateUpdate=TDate();
		obj.lastDateUpdate.setSeconds(obj.lastDateUpdate.getSeconds() + moreTime);
		obj.data={'sun':obj.sun,'moonfase':obj.moonFase.phase,'latitude':obj.latitude,'longitude':obj.longitude,'lastDateUpdate':obj.lastDateUpdate,'typeLocation':obj.typeLocation};
		sunmoondata=obj.data;
	};
	this.obtainLocale=function(){
		if(typeof(tizen) != 'undefined' && typeof(tizen.systeminfo) != 'undefined'){
			tizen.systeminfo.getPropertyValue("LOCALE", function(locale){
				if(typeof locale != 'undefined' && typeof locale.country != 'undefined' && locale.country.length > 0){
					if(locale.country.indexOf("_")>=0){
						var split=locale.country.split("_");
						if(typeof split[1] != 'undefined'){
							obj.locale=split[1];
							obj.latitude=countries[obj.locale].latitude;
							obj.longitude=countries[obj.locale].longitude;
						}
					}
				}
				obj.typeLocation="L";
				obj.setPositionData(18000);
			});
		}else{
			obj.typeLocation="L";
			obj.setPositionData(18000);
		}
	};
	
	this.obtainFirstLocale=function(){
		if(typeof(tizen) != 'undefined' && typeof(tizen.systeminfo) != 'undefined'){
			tizen.systeminfo.getPropertyValue("LOCALE", function(locale){
				if(typeof locale != 'undefined' && typeof locale.country != 'undefined' && locale.country.length > 0){
					if(locale.country.indexOf("_")>=0){
						var split=locale.country.split("_");
						if(typeof split[1] != 'undefined'){
							obj.locale=split[1];
							obj.latitude=countries[obj.locale].latitude;
							obj.longitude=countries[obj.locale].longitude;
						}
					}
				}
				obj.typeLocation="L";
				obj.setPositionData(8);
			});
		}else{
			obj.typeLocation="L";
			obj.setPositionData(8);
		}
	};
	
	this.obtainGpsPosition = function() {
		if (tDate.getTime() > obj.lastDateUpdate.getTime()) {
			obj.obtainLocale();
			obj.watchDate = TDate();
			obj.watchDate.setSeconds(obj.watchDate.getSeconds() + 250);
			obj.clear();

			if (typeof (navigator.geolocation) != 'undefined') {
				obj.watchId = navigator.geolocation.getCurrentPosition(
					function(position) {
						obj.latitude = position.coords.latitude;
						obj.longitude = position.coords.longitude;
						obj.typeLocation = "S";
						obj.setPositionData(18000);
					}, function(error) {
						// alert(JSON.stringify(error));
						obj.obtainLocale();
					}, {
						maximumAge : 6000,
						timeout : 120000,
						enableHighAccuracy : false
					});
			}
		}
	};
	
	this.checkClear = function(){
		if(obj.watchDate != null && tDate.getTime() > obj.watchDate.getTime()){
			obj.watchDate=null;
			obj.clear();
		}
	}
	
	this.clear=function(){
		if (typeof (navigator.geolocation) != 'undefined') {
			navigator.geolocation.clearWatch(obj.watchId);
		}
	};
};
