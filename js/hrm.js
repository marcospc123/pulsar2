function HRM(){
	var obj=this;
	obj.heartRate=0;
	obj.rRInterval=0;
	obj.measuring=false;
	obj.numChanges=0;
	obj.HRMMotion=false;
	
	
	/*
	this.HRMAvailableSensor = (typeof webapis != 'undefined')?webapis.sensorservice.getAvailableSensors():[];
	if(this.ultravioletAvailableSensor.length>0){
		if(this.ultravioletAvailableSensor.indexOf("ULTRAVIOLET") != -1){
			this.ultravioletSensor = (typeof webapis != 'undefined')?webapis.sensorservice.getDefaultSensor("ULTRAVIOLET"):false;
		}else{
			this.ultravioletSensor = false;
		}
	}else{
		this.ultravioletSensor=false;
	}*/
	//window.webapis.motion.getMotionInfo("HRM",obj.onchangedCB,obj.onchangedCB);
	
	
	
	
	obj.onchangedCB=function(hrmInfo) {
		if(hrmInfo.heartRate > 0) {
			if(obj.numChanges==0){
				obj.heartRate=hrmInfo.heartRate;
				obj.rRInterval=hrmInfo.rRInterval;
				obj.numChanges++;
			}else if(obj.numChanges==5){
				obj.numChanges=0;
			}else{
				obj.numChanges++;
			}
		} else {
			obj.heartRate=0;
			obj.rRInterval=0;
		}
	}
	
	obj.getHeartRate=function(hrmInfo) {
		return obj.heartRate;
	}
	obj.getrRInterval=function(hrmInfo) {
		return obj.rRInterval;
	}
	
	obj.measureHRM=function() {
		if (!obj.measuring) {
			obj.measuring=true;
			try {
				window.webapis.motion.start("HRM", obj.onchangedCB);
			}catch(err) {
				obj.heartRate=-1;
				obj.rRInterval=-1;
			}
		}
	}
	
	obj.stop=function(){
		obj.measuring=false;
		window.webapis.motion.stop("HRM");
	}
	
	obj.pageVisibilityHandler = function() 
	{
		if (document.hidden) 
		{
			//checkHrm=false;
			if(obj.measuring){
				obj.stop();
			}
		}else{
			var checkboxHRM = document.getElementById('checkboxHRM');
			if(checkHrm){
				checkboxHRM.checked = true;
			}else{
				checkboxHRM.checked = false;
				if(obj.measuring){
					obj.stop();
				}
			}
		}
	}
	document.addEventListener("visibilitychange", obj.pageVisibilityHandler, false);
	
	try {
		window.webapis.motion.start("HRM", function(){
			obj.HRMMotion=true;
			window.webapis.motion.stop("HRM");
		});
		obj.HRMMotion=true;
	}catch(err) {
		obj.HRMMotion=false;
	}
	
};