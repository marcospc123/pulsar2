function isEmpty(obj) {
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop))
			return false;
	}
	return true;
}


function opacity (p) { // p move from 0 to 1
  context.globalAlpha = 1*p;
}

function animate (render, duration, easing) {
  var start = tDate.getTime();
  (function loop () {
    var p = (tDate.getTime()-start)/duration;
    if (p > 1) {
      render(1);
    }
    else {
      requestAnimationFrame(loop);
      render(easing(p));
    }
  }());
}

BezierEasing.css = {
  "ease":        BezierEasing(0.25, 0.1, 0.25, 1.0), 
  "linear":      BezierEasing(0.00, 0.0, 1.00, 1.0),
  "ease-in":     BezierEasing(0.42, 0.0, 1.00, 1.0),
  "ease-out":    BezierEasing(0.00, 0.0, 0.58, 1.0),
  "ease-in-out": BezierEasing(0.42, 0.0, 0.58, 1.0)
};

//remove elements DOM
Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 0, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function zeroFill( number, width )
{ 
	return number;
	var s = "000000000" + number;
    return s.substr(s.length-width);
	
}

//TIZEN DATE
function TDate(){
	var date= (typeof tizen !=='undefined')?tizen.time.getCurrentDateTime():new Date();
	
	/*if(isScreenSmall){
		return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(),date.getMilliseconds());
	}else{
		return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(),date.getUTCMilliseconds());
	}*/
	var ms=date.getMilliseconds();
	var s=date.getSeconds();
	
	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), s,ms);
}


/*tizen.TZDate.prototype.getTime = function() {
	var day=zeroFill(this.getDate(),2);
	var month=zeroFill(this.getMonth(),2);
	var year=zeroFill(this.getFullYear(),4);
	var hours=zeroFill(this.getHours(),2);
	var minutes=zeroFill(this.getMinutes(),2);
	var seconds=zeroFill(this.getSeconds(),2);
	var milliseconds=zeroFill(this.getMilliseconds(),3);
	
	var strDate=day+"/"+month+"/"+year+" "+hours+":"+minutes+":"+seconds+" "+milliseconds;
	
	//var parts = strDate.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2}) (\d{3})/);
	
	//console.log(strDate);
	//console.log(Date.UTC(+parts[3], parts[2], +parts[1], +parts[4], +parts[5], +parts[6], +parts[7]));
	//console.log(Date.now());
	
	return Date.UTC(year, month, day, hours, minutes, seconds, milliseconds);
	//return Date.UTC(+parts[3], parts[2], +parts[1], +parts[4], +parts[5], +parts[6], +parts[7]);
}*/

/*function TDateGetTime(){
	var day=zeroFill(tDate.getDate(),2);
	var month=zeroFill(tDate.getMonth(),2);
	var year=zeroFill(tDate.getFullYear(),4);
	var hours=zeroFill(tDate.getHours(),2);
	var minutes=zeroFill(tDate.getMinutes(),2);
	var seconds=zeroFill(tDate.getSeconds(),2);
	var milliseconds=zeroFill(tDate.getMilliseconds(),3);
	
	var strDate=day+"/"+month+"/"+year+" "+hours+":"+minutes+":"+seconds+" "+milliseconds;
	
	//var parts = strDate.match(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2}) (\d{3})/);
	
	//console.log(strDate);
	//console.log(Date.UTC(+parts[3], parts[2], +parts[1], +parts[4], +parts[5], +parts[6], +parts[7]));
	//console.log(Date.now());
	return Date.UTC(year, month, day, hours, minutes, seconds, milliseconds);
	//return Date.UTC(+parts[3], parts[2], +parts[1], +parts[4], +parts[5], +parts[6], +parts[7]);
}*/


tizen.TZDate.prototype.getTime = function() {
	//var date=new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds(),this.getMilliseconds()));
	var date=new Date(Date.UTC(this.getUTCFullYear(), this.getUTCMonth(), this.getUTCDate(), this.getUTCHours(), this.getUTCMinutes(), this.getUTCSeconds(),this.getUTCMilliseconds()));
	return date.getTime();
}

function boolRandom(){
	//var timestamp=tDate.getTime();
	//return (!(Math.random()+.5|0)) && (Math.random()<.5) && (!(+timestamp%3)) && (!(Math.random()+.5|0)) && (Math.random()<.5) && (!(+timestamp%2));
	
	return (Math.random()<.5) && !!Math.floor(Math.random() * 2) ;
}



